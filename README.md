# Écriture académique : Markdown + Zotero
Supports pour un atelier autour d'outils d'écriture numérique et de gestion de références bibliographiques pour des chercheurs en design (association Design en Recherche).

## Demande initiale
Présenter des solutions d'écriture académique basées sur le langage de balisage léger Markdown, incluant une gestion de références bibliographiques.

## Plan de l'atelier
Plan indicatif de la journée :

- partie 0 : tour de table et introduction ;
- partie 1 : Markdown, rappels ;
- partie 2 : Zotero, les bases et les fonctions avancées ;
- partie 3 : des outils d'écriture (numériques).

## Présentations en ligne
https://presentations.quaternum.net/ecriture-academique-markdown-zotero/

## Supports
Les supports sont disponibles ici sous la licence Creative Commons CC BY-SA et au format AsciiDoc.
