= Partie 1 : Markdown, rappels
:revealjs_theme: white
:imagesdir: images

== Les origines de Markdown

=== Simplifier l'écriture numérique
> Markdown is a text-to-HTML conversion tool for web writers. Markdown allows you to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML).
>
> John Gruber, https://daringfireball.net/projects/markdown/

[NOTE.speaker]
--
Contexte de l'époque : écrire directement en HTML ou passer par des langages de balisage comme LaTeX.
--

=== Un langage de balisage léger
> Markdown est un langage de balisage léger créé en 2004 par John Gruber avec Aaron Swartz. Son but est d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.
>
> Wikipédia, https://fr.wikipedia.org/wiki/Markdown

[NOTE.speaker]
--
L'objectif est donc de remplacer soit les processus lourds comme LaTeX, HTML ou les traitements de texte (comme Word), par une structuration basée sur des balises facilement compréhensible par les humaines, et transformables en d'autres formats par les machines.
--

=== Un langage qui peut se suffire à lui-même
Markdown permet de *distinguer la structure et la mise en forme*.

Markdown est *compréhensible*.

Markdown est *résilient*.


== Rappel du fonctionnement

=== Les balises
Liste complète : https://commonmark.org/help/

=== Manipulations (10 minutes)
https://commonmark.org/help/tutorial/

=== Afficher Markdown
Le premier outil indispensable est un éditeur interprétant le Markdown pour un affichage optimal : éditeur de texte, logiciel dédié, application en ligne.
Quelques exemples :
[.step]
* Atom, VS Code, Sublime Text : avec des extensions ;
* (Vim, Emacs, Orgmode) ;
* iA Writer, Typora, Abricotine ;
* HackMD, GitHub, GitLab.

=== Exercice (20 minutes)
Reprenez l'article que vous avez sélectionné et transformez-le en Markdown dans l'éditeur/logiciel de votre choix (par défaut : HackMD) puis envoyez-le moi (antoine@quaternum.net).

=== Transformer Markdown
De nombreux programmes ou de logiciels permettent de transformer Markdown en beaucoup d'autres choses (HTML, PDF, .docx, Docbook, XML, etc.) :
[.step]
* directement dans les logiciels d'édition (iA Writer, Typora) ;
* avec des éditeurs de texte/code via des extensions ;
* Pandoc : couteau suisse.

=== Exercice (20 minutes)
En utilisant Pandoc, effectuez les transformations suivantes :

* Markdown → HTML
* Markdown → .docx
* Markdown → PDF


== La souplesse d'un langage non normalisé
[NOTE.speaker]
--
Markdown a quelques bases sur lesquelles tous le monde s'accorde, mais il y a beaucoup d'ambiguïtés lorsque l'on rentre dans les détails.
--

=== Interpréter Markdown
Markdown est un format voué à être transformé, et c'est cette transformation qui pose question.

Pour comprendre cette épineuse question : https://johnmacfarlane.net/babelmark2/

=== Des limites (pour nous académiciens)
Certaines implémentations ne prennent pas en compte les notes de bas de page.
La gestion bibliographique nécessite d'utiliser des extensions.


== Quelques outils compatibles

=== Les éditeurs de texte
Atom et VS Code : *légèreté*, *customisation*, *dépouillement*, _aridité_.

=== Les logiciels dédiés
iA Writer (payant), Typora : *facilité d'utilisation*, _manque de

=== Les systèmes de publication
pagedown (https://github.com/rstudio/pagedown), ReLaXed (https://github.com/RelaxedJS/ReLaXed).

Exemple plus complexe : Atom + Jekyll + Jekyll-scholar + Jekyll-microtypo + GitLab + Netlify + Forestry.io (modèle disponible ici : https://github.com/michaelravedoni/jekyll-book-framework)

[%notitle]
=== Les systèmes de publication (bis)
image::schema-getty.svg[size=contain]
